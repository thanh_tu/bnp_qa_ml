1. Q&A session on ELECTRA model: [video](https://www.youtube.com/watch?v=QWu7j1nb_jI), [paper](https://openreview.net/pdf?id=r1xMH1BtvB)

2. Q&A session on Toward theoretical understanding of deep learning : [video](https://www.youtube.com/watch?v=KDRN-FyyqK0&feature=youtu.be), [papers](https://unsupervised.cs.princeton.edu/deeplearningtutorial.html)

3. Unsupervised Data Augmentation for consistency training [video](https://www.youtube.com/watch?v=fgwurrihq4A), [paper](https://arxiv.org/abs/1904.12848)

4. Graph representation learning [video](https://www.youtube.com/watch?v=YrhBZUtgG4E), [paper](https://www-cs.stanford.edu/people/jure/pubs/graphrepresentation-ieee17.pdf)

5. Winning The Lottery Ticket Hypothesis  [video](https://www.youtube.com/watch?time_continue=7&v=s7DqRZVvRiQ&feature=emb_logo), [paper](https://arxiv.org/pdf/1803.03635.pdf) 

6. Interpretability Beyond Feature Attribution [video](https://www.youtube.com/watch?v=Ff-Dx79QEEY) [paper](https://arxiv.org/abs/1711.11279)

7. Neural Semi-supervised Learning under Domain Shift [video](https://www.youtube.com/watch?v=tpAr5-Y4JxU) [paper](https://arxiv.org/abs/1804.09530)

8. Flexible Neural Networks and the Frontiers of Meta-Learning [video](https://www.youtube.com/watch?v=kmbPnsgHxz4) [paper](https://arxiv.org/pdf/1806.03836.pdf)




